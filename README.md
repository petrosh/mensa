# Mensa

## Features

* show current plan of mensa
* filter by vegetarian or vegan food
* show or hide price
* show any number of spaces between food and price
* combined with [conky][0], this plan appears on your desktop
* [new] now saving plan for one week, so access is much faster

## Requirements

* Linux
* Internet connection

## Installation

after download add path to mensa script folder to your $PATH in .bashrc

    export PATH=$PATH:/path/to/mensa

or add a link to the script to one of your $PATH folders

    ln -s /path/to/mensa your_link_name_in_path_folder


## Help

run mensa -h for help

[0]: https://wiki.archlinux.org/index.php/Conky