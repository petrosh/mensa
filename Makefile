.PHONY: all clean run

all: Mensa.jar

clean:
	rm -f Mensa.class Mensa.jar

%.class: %.java
	javac $<

Mensa.jar: Mensa.class
	jar -cfe Mensa.jar Mensa Mensa.class

run: Mensa.jar
	mensa
