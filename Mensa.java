/**
 * Mensa script in Java version
 *
 * @author Robin Heinbockel
 * @version 1.0
 *
 */

import java.io.*;
import java.net.*;
import java.util.*;

public class Mensa {

	public static final String[] WEEKDAY = {"Montag", "Dienstag", "Mittwoch", "Donnerstag", "Freitag", "Samstag", "Sonntag"};
	public static final String[] TIME = {"", "Mittagsmensa", "Abendmensa"};
	public static final String[] VEG  = {" ", "v", "V"};

//	private static File root;
	private static File save;

	private static int day;
	private static String lastSunday;
	private static int time; // 0 beide, 1 mittags, 2 abends
	private static int spaces;
	private static int veg;
	private static boolean price;
	private static boolean refresh;
	private static char space = ' ';

	public static void main(String[] args) {

		if(args.length >= 7) {
			lastSunday = args[0];
			day = Integer.parseInt(args[1]) - 1;
			time = Integer.parseInt(args[2]);
			spaces = Integer.parseInt(args[3]);
			veg = Integer.parseInt(args[4]);
			price = args[5].equals("1") || args[5].equals("true");
			refresh = args[6].equals("true");
			if(args.length > 7) {
				space = args[7].charAt(0);
			}
		} else {
			System.out.println("wrong argument count: " + args.length + ", requires 7 arguments.");
			System.exit(-1);
		}

		try{
			save = new File("mensa.txt");
			if(!save.exists())
				save.createNewFile();
		} catch(Exception e) {
			System.out.println("Error while checking save file.");
			e.printStackTrace();
			System.exit(-1);
		}

		if(!isCorrectWeek() || refresh) {
			System.out.println("getting some fresh food...");
			calculateForWeek();
		}

		show();
	}

	private static boolean isCorrectWeek() {
		try {
			BufferedReader br = new BufferedReader(new FileReader(save));
			String weekOfSave = br.readLine();
			return lastSunday.equals(weekOfSave);
		} catch(Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	private static void calculateForWeek() {
		try {
			URL url = new URL("http://www.stw-on.de/braunschweig/essen/menus/mensa-1");
			BufferedReader br = new BufferedReader(new InputStreamReader(url.openStream()));
			String line = "";
			int d = -1; // day of week
			int t = 1; // time
			String outStream = lastSunday + "\n";
			while((line = br.readLine()) != null) {
				if(line.matches(".*class=\"swbs_speiseplan\".*")) {
					// some magic for calculating current day of week and time
					d += t;
					t = (t + 1) % 2;
					outStream += d + "/" + (t + 1) + "\n";
				} else if(line.matches(".*swbs_speiseplan_other\">[A-Z0-9].*")) { // food
					outStream += line.split(">")[1].split("<")[0] + "\n";
				} else if(line.matches(".*swbs_speiseplan_price_s\">.*")) { // price
					outStream += line.split(">")[1].split("<")[0] + "\n";
				} else if(line.matches(".*hg-icon/(SCHW|GEFL|RIND|FISH).*")) { // meat
					outStream += "0\n";
				} else if(line.matches(".*hg-icon/VEGA.*")) { // vegan
					outStream += "2\n";
				} else if(line.matches(".*hg-icon/VEGT.*")) { // vegetarian
					outStream += "1\n";
				}
			}
			outStream = parse(outStream);
			BufferedWriter bw = new BufferedWriter(new FileWriter(save));
			bw.write(outStream, 0, outStream.length());
			bw.close();
		} catch(MalformedURLException murle) {
			System.out.println("invalid url");
		} catch(UnknownHostException uhe) {
			System.out.println("could not connect to server");
		} catch(IOException ioe) {
			System.out.println("could not write to file");
		} catch(Exception e) {
			e.printStackTrace();
		}
	}

	private static void show() {
		try{
			BufferedReader br = new BufferedReader(new FileReader(save));
			String line = "";
			boolean reached = false;
			System.out.println("---- " + WEEKDAY[day] + " -- " + TIME[time] + " ----");
			while((line = br.readLine()) != null) {
				if(line.equals(day + "/" + time)) {
					reached = true;
					continue;
				}
				if(reached) {
					if(line.matches("\\d*/\\d*"))
						reached = false;
					else{ // make some fancy output
						String p = br.readLine();
						int v = 0;
						try{ // in case of a missing veg symbol
							br.mark(100);
							v = Integer.parseInt(br.readLine());
						} catch (Exception e) {
							br.reset();
						}
						if(v < veg)
							continue;
						if(veg == 0) // only show veg status, if everything is shown
							System.out.print(VEG[v] + " ");
						else
							System.out.print("  ");
						System.out.print(line + spaces(line));
						if(price)
							System.out.print(p);
						System.out.println();
					}
				}
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
	}

	private static String spaces(String l) {
		//String s = "";
		//while(spaces - 8 > l.length() + s.length())
		//	s += " ";
		//return s;
		return fill(spaces - 12 - l.length(), space);
	}

	private static String fill(int count, char c) {
		String s = "";
		while(s.length() < count)
			s += c;
		return s;
	}

	private static String parse(String in) {
		in = in.replaceAll("&auml;", "ä");
		in = in.replaceAll("&ouml;", "ö");
		in = in.replaceAll("&uuml;", "ü");
		in = in.replaceAll("&Auml;", "Ä");
		in = in.replaceAll("&Ouml;", "Ö");
		in = in.replaceAll("&Uuml;", "Ü");
		in = in.replaceAll("&#038;", "&");
		in = in.replaceAll("&#8220;", "\"");
		in = in.replaceAll("&#8221;", "\"");
		in = in.replaceAll("&szlig;", "ß");
		in = in.replaceAll("&euro;", "€");
		return in;
	}
}

